#include <Foundation/Foundation.h>

@protocol NossoBatePapo
- (id) autentica: (NSString*) usuario comSenha: (NSString*) senha;
- (void) enviaMensagem: (NSString*) mensagem comIdentificador: (id) token;
- (void) publicaStatus: (NSString*) status comIdentificador: (id) token;
- (void) logoff: (id) token;
@end
