#include <Foundation/Foundation.h>

@interface DataShow : NSObject
@end

@implementation DataShow
- (void) publicaMensagem: (NSString*) mensagem
{
  NSLog (@"%@", mensagem);
}
@end

int main (int argc, char **argv)
{
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  //Coloque o código deste exercício a partir daqui



  //objeto exposto
  DataShow *dataShow = [[DataShow alloc] init];

  //NSConnection eh responsavel por expor um objeto
  NSSocketPort *port = [[NSSocketPort alloc] init];
  NSConnection *theConnection = [NSConnection connectionWithReceivePort: port
							       sendPort: port];
  [theConnection setRootObject: dataShow];

  //Tentando registrar o objeto a ser exposto com um nome
  if ([theConnection registerName:@"VideoProjetor"
		   withNameServer: [NSSocketPortNameServer sharedInstance]] == NO) {
    NSLog(@"Impossivel de expor este objeto.");
  } else {
    NSLog(@"Objeto exposto.");
  }

  //Rodar o programa
  [[NSRunLoop currentRunLoop] run];
  [port release];


  //Programa termina
  [pool release];
  return 0;
}
