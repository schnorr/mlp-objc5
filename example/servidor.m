#include <Foundation/Foundation.h>
#include "BackupServerProtocol.h"

@interface BackupServer : NSObject <BackupServerProtocol>
@end

@implementation BackupServer
- (void) backup 
{
  int i;
  NSLog(@"backuping : ");
  for(i = 0; i < 5; i++) {
    NSLog(@".");
    [NSThread sleepForTimeInterval:1.0];
  }
}
@end


/**
 * Codigo do servidor
 */
int main (int argc, const char * argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //O objeto que sera exposto eh o seguinte:
  BackupServer *backupServer = [[BackupServer alloc] init];

  //NSConnection eh responsavel por expor um objeto
  NSSocketPort *port = [[NSSocketPort alloc] init];
  NSConnection *theConnection = [NSConnection connectionWithReceivePort: port
							       sendPort: port];
  [theConnection setRootObject: backupServer];

  //Tentando registrar o objeto a ser exposto com um nome
  if ([theConnection registerName:@"MeuServidorDeBackup"
		   withNameServer: [NSSocketPortNameServer sharedInstance]] == NO) {
    NSLog(@"Impossivel de expor este objeto.");
  } else {
    NSLog(@"Objeto exposto.");
  }

  //Rodar o programa
  [[NSRunLoop currentRunLoop] run];
  [port release];
  [pool release];
  return 0;
}
