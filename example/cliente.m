#include <Foundation/Foundation.h>
#include "BackupServerProtocol.h"

/** 
 * Codigo do Cliente
 */
int main (int argc, const char * argv[]) {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  //Conectar-se a maquina localhost e tentar obter um proxy para MeuServidorDeBackup
  id backupServer = [NSConnection
	       rootProxyForConnectionWithRegisteredName: @"MeuServidorDeBackup"
						   host: @"*"
					usingNameServer: [NSSocketPortNameServer sharedInstance]];

  //Setar qual eh o protocolo obedecido pelo objeto do servidor (opcional)
  [backupServer setProtocolForProxy:@protocol(BackupServerProtocol)];

  //Enviar a mensagem backup (ou chamar o metodo backup) do objeto remoto
  [backupServer backup];

  //Fim do programa
  NSLog(@"Work finished.");

  [pool release];
  return 0;
}
